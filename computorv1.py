#!/usr/bin/env Python

import sys, re, string
#
## begin import for BONUS
#
from fractions import Fraction
from graphics import *
from math import acos
from math import cos
from math import pi
#
## end import for BONUS
#
width = 800
height = 600
min_x = -5
max_x = 5
min_y = -5
max_y = 5
precision = 0.01

red = "\033[31m"
none = "\033[0m"

def main(av1, av2):
	if av2 == "-f":
		function = convert_to_fract
	else:
		function = str
	parse_error(av1)
	s = replace_natural(av1)
	max = find_max_deg(s)
	dict = parse (s, max)
	draw = Draw(dict , max)
	if max <= 3:
		solve (dict, function, draw)
	else:
		print ("The polynomial degree is stricly greater than 3, I can't solve.")
	draw.draw()

class Draw:
	def __init__(self, dict, max):
		self.height = height
		self.width = width
		self.precision = precision
		self.solve = []
		self.lst = []
		self.define_equation(dict, max)

	def define_equation(self, dict_e, max_deg):
		self.dict = dict_e
		self.max_deg = max_deg
		self.calc_points(min_x,max_x,min_y,max_y)

	def convert_x(self, x):
		return (x - self.min_x) * (self.width / (self.max_x - self.min_x))

	def convert_y(self, y):
		tmp = (y - self.min_y) * (self.height / (self.max_y - self.min_y))
		return height - tmp

	def draw(self):
		win = GraphWin("Graph", self.width, self.height)
		abs = Line(Point(0, self.convert_y(0)), Point(win.getWidth(), self.convert_y(0)))
		abs.setFill("blue")
		Text(Point(self.convert_x(self.min_x) + 10, self.convert_y(0) + 10), self.min_x).draw(win)
		Text(Point(self.convert_x(self.max_x) - 10, self.convert_y(0) + 10), self.max_x).draw(win)
		Text(Point(self.convert_x(0) + 10, self.convert_y(self.min_y) - 10), self.min_y).draw(win)
		Text(Point(self.convert_x(0) + 10, self.convert_y(self.max_y) + 10), self.max_y).draw(win)
		for x in range(self.min_x + 1, self.max_x):
			Line(Point(self.convert_x(x), self.convert_y(0) - 1), Point(self.convert_x(x), self.convert_y(0) +1)).draw(win)
		ord = Line(Point(self.convert_x(0), 0), Point(self.convert_x(0), self.height))
		ord.setFill("blue")
		for y in range(self.min_y + 1, self.max_y):
			Line(Point(self.convert_x(0) - 1, self.convert_y(y)), Point(self.convert_x(0) + 1, self.convert_y(y))).draw(win)
		abs.draw(win)
		ord.draw(win)
		for i in self.lst:
			Point(self.convert_x(i[0]), self.convert_y(i[1])).draw(win)
		for j in self.solve:
			j.draw(win)
		win.getMouse()
		win.close()

	def add_solve(self, x):
			tmp = Point(self.convert_x(x), self.convert_y(0))
			tmp.setFill("red")
			self.solve.append(tmp)

	def calc_points(self, min_x, max_x, min_y, max_y):
		self.min_x = min_x
		self.max_x = max_x
		self.min_y = min_y
		self.max_y = max_y
		self.lst = []
		x = min_x
		while x <= max_x:
			y = 0
			j = 0
			while (j <= self.max_deg):
				if (j in self.dict.keys()):
					y += self.dict[j] * (x ** j)
				j += 1
			if (y >= min_y and y <= max_y):
				self.lst.append([x, y])
			x += self.precision

def solve (dict, function, draw):
	if (dict[3] == 0):
		solve_2_deg(dict[2], dict[1], dict[0], function, draw)
	else:
		solve_3_deg(dict, function, draw)

def solve_3_deg(dict, function, draw):
	if (dict[2] == 0 and dict[1] == 0):
		x1 = (-dict[0] / dict[3]) ** (1./3.)
		real = -x1 / 2.
		ireal = (3. ** (1./2.)) / 2. * x1
		draw.add_solve(x1)
		print ("The three solutions are:")
		print (function(x1))
		print (function(real) + " + i * " + function(ireal))
		print (function(real) + " - i * " + function(ireal))
	else:
		a0 = dict[0] / dict[3]
		a1 = dict[1] / dict[3]
		a2 = dict[2] / dict[3]
		a3 = a2 / dict[3]
		p = a1 - (a2 * a3)
		q = a0 - (a1 * a3) + (2. * (a3 ** 3.))
		delta = ((q / 2.) ** 2.) + ((p / 3.) ** 3.)
		if (delta > 0):
			tmp = ((-q / 2.) + (delta ** (1./2.)))
			if (tmp > 0):
				w = tmp ** (1./3.)
			else:
				w = (-tmp) ** (1./3.)
			if (tmp < 0):
				w = -w
			x1 = w - (p / (3. * w)) - a3
			real = ((a2 + x1)/(-2.))
			ireal = ((3 ** (1./2.))/2.) * (w + (p / (3. * w)))
			draw.add_solve(x1)
			print ("Discriminant strictly positive (Delta = " + function(delta)+ "), the three solutions are:")
			print (function(x1))
			print (function(real) + " + i * " + function(ireal))
			print (function(real) + " - i * " + function(ireal))
		elif (delta == 0):
			x1 = ((3. * q) / p) - a3
			x2 = ((-3. * q) / (2. * p)) - a3
			print ("The two solutions are:")
			print (function(x1))
			print (function(x2))
			draw.add_solve(x1)
			draw.add_solve(x2)
		else:
			u = 2. * (((-p) / 3.) ** (1./2.))
			v = ((-q) / (2. * (((-p) / 3.) ** (3./2.))))
			t = (acos(v) / 3.)
			x1 = (u * cos(t)) - a3
			draw.add_solve(x1)
			x2 = (u * cos(t + ((2. * pi) / 3.))) - a3
			draw.add_solve(x2)
			x3 = (u * cos(t + ((4. * pi) / 3.))) - a3
			draw.add_solve(x3)
			print ("Discriminant strictly negative (Delta = " + function(delta) + "), the three solutions are:")
			print (function(x1))
			print (function(x2))
			print (function(x3))



def solve_2_deg (a, b, c, function, draw):
	if a == 0 and b != 0:
		draw.add_solve((-c) / b)
		print ("The Solution is: " + function((-c) / b))
	elif a != 0:
		delta = (b ** 2.) - (4. * a * c)
		real = (-b / (2. * a));
		if delta == 0:
			print ("The solution is:")
			print (function(real))
			draw.add_solve(real)
		elif delta > 0:
			ireal = ((delta ** (1./2.)) / (2. * a))
			print ("Discriminant is strictly positive (Delta= " + function(delta) + "), the two solutions are:")
			print (function(real + ireal))
			print (function(real - ireal))
			draw.add_solve(real + ireal)
			draw.add_solve(real-ireal)
		elif delta < 0:
			ireal = (((-delta) ** (1./2.)) / (2. * a))
			print ("Discriminant is strictly negative (Delta= " + function(delta) + "), the two solutions complex are:")
			print (function(real) + " + i * " + function(ireal))
			print (function(real) + " - i * " + function(ireal))
	elif a == 0 and b == 0 and c == 0:
		print ("All reals numbers are solutions")
	else:
		print ("there is no solution.")

def find_max_deg(s):
	patern = "[Xx]\s*[\^]?\s*(\d*)"
	m = re.compile(patern)
	lst = m.findall(s)
	res = 0
	for i in lst:
		if len(i) == 0 and res < 1:
			res = 1
		elif int(i) and int(i) > res:
			res = int(i)
	return res

def max_in_dict(dict, max):
	i = max
	res = 0
	while i >= 0:
		if i in dict.keys() and dict[i] > 0:
			return i
		i -= 1
	sys.exit("wrong degre.")

def find_by_degre (s, deg):
	patern = "([\+\-]?\s*\d*[\.\,]?\d*)\s*[\*]?\s*[Xx]\s*[\^]?\s*" + str(deg) + "|\s*[Xx]\s*[\^]?\s*[\*]?\s*" + str(deg) + "\s*([\+\-]?\s*\d*[\.\,]?\d*)"
	m = re.compile(patern)
	lst = m.findall(s)
	res = 0.
	for i in lst:
		already = 0
		for j in i:
			if len(j) == 0 and already == 0:
				already = 1
			elif len(j) == 0:
				res += 1
			else:
				try:
					res += string.atof("".join(string.split(j, " ")))
				except ValueError:
					if (j[0] == "+"):
						res += 1
					elif (j[0] == "-"):
						res -= 1
	return res

def absolute(i):
	if i < 0:
		return -i
	return i

def convert_to_fract(f):
	return (str(Fraction.from_float(f)))

def parse (s, max):
	eq = s.split("=")
	if len(eq) > 2:
		sys.exit ("ERROR: equation have 1 sign '=' max")
	dict = {}
	max = find_max_deg(s)
	i = 0
	while i <= max:
		left  = find_by_degre (eq[0], i)
		if len(eq) == 2:
			right = find_by_degre (eq[1], i)
		else:
			right = 0
		dict[i] = left - right
		i += 1
	i = 0
	reduced = ""
	first = 0
	while i <= max:
		if (i in dict.keys() and dict[i] != 0):
			if (len(reduced) == 0):
				if dict[i] != 1:
					reduced += str(dict[i])
					first = 2
			else:
				first += 1
				if (dict[i] < 0):
					reduced += " -"
				else:
					reduced += " +"
				if (absolute(dict[i]) != 1):
					reduced += " " + str(absolute(dict[1])) + " *"
			if (first > 1):
				reduced += " "
			if i > 0:
				reduced += "X^" + str(i)
			elif i == 1:
				reduced += "X"
		i += 1
	if (len(reduced) == 0):
		reduced += "0"
	print (red + "Reduced form: " + none + reduced + " = 0")
	print (red + "Polynomial degree: " + none+ str(max))
	i = 0
	while (i <= 3):
		if (i not in dict.keys()):
			dict[i] = 0
		i += 1
	return dict

def parse_error(s):
	pattern = r"[Xx]\s*[\^]?\s*(\d*[\.\,])\s*|[Xx]\s*[\^]?\s*(\-)|([a-wy-zA-WY-Z])|([\-\+\,\.\\\^]\s*[\-\+\,\.\\\^])|([xX]{2,})"
	m = re.compile(pattern)
	if (len(m.findall(s)) != 0):
		sys.exit("SYNTAX ERROR")

def in_order(s):
	tmp = s.split("*")
	if (len(tmp) == 2 and tmp[0].find("X") != -1):
		if (tmp[0][0] == '+' or tmp[0][0] == '-'):
			sign = tmp[0][0]
			o = sign + tmp[1]
			x = "".join(tmp[0].split(sign))
			s = o + x
		else:
			s = tmp[0] + tmp[1]
		s = s.replace("++", "+")
		s = s.replace("--", "+")
		s = s.replace("+-", "-")
		s = s.replace("-+", "-")
	return s

def replace_natural(s):
	s = s.replace(" ", "")
	s = s.replace("x", "X")
	s = s.replace("X^", "X")
	s = s.replace("+"," +")
	s = s.replace("-"," -")
	s = s.replace("=", " =")
	s = s.replace("* ", "*")
	tmp = s.split()
	tmp2 = []
	for i in tmp:
		if (i != "="):
			i = in_order(i)
			if i.find("X") == -1:
				i += "X0"
			if (i.find("X") + 1) == len(i):
				i += "1"
		tmp2 += i
	s = "".join(tmp2)
	s = s.replace("+X", "+1X")
	s = s.replace("-X", "-1X")
	return s

ac = len(sys.argv)
if ac == 3:
	main(sys.argv[1], sys.argv[2])
elif ac == 2:
		main(sys.argv[1], "")
else:
	sys.exit (red + "usage: " + none + "python " + __file__ + " [equation] [-f]")
